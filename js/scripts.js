const playPause = document.getElementById("play");

const forward = document.getElementById("forward");
const backward = document.getElementById("backward");

const selector = document.getElementById("selector");

const pauseCheck = document.getElementById("pauseCheck");

var numberOfSongs = 8 //Especificar el numero de canciones en la tienda

const beatList = ["audio/song.mp3", "audio/beat2.mp3", "audio/beat3.mp3", "audio/beat4.mp3"];
var index = 1;

var text = '{ "tracks": [' +
  '{"audio": "audio0", "title": "Epic", "img": "../img/epic.jpg"},'+
  '{"audio": "audio1", "title": "World order", "img": "../img/audioimage.jpg"},'+
  '{"audio": "audio2", "title": "A new day", "img": "../img/worldorder.jpg"},'+
  '{"audio": "audio3", "title": "Piano", "img": "../img/piano.jpg"} ]}';

var decemberTracks = '{ "tracks": [' +
  '{"audio": "audio4", "title": "Dark", "img": "../img/dragon2.jpg"},'+
  '{"audio": "audio5", "title": "Sax", "img": "../img/beat5.jpg"},'+
  '{"audio": "audio6", "title": "Progressive", "img": "../img/progressive.jpg"},'+
  '{"audio": "audio7", "title": "Great Canyon", "img": "../img/canyon.jpg"} ]}';

var december = JSON.parse(decemberTracks);
var obj = JSON.parse(text);

var selectedBeatpack = obj;

//console.log(obj.tracks[0].title)

var currentAudio = "audio0";
let audio = document.getElementById(currentAudio);

let audioNbeat = document.getElementById("audio"+index+"beat");

initialize(obj);

function initialize(json) {
  for(var i = 0; i<numberOfSongs; i++){
    if(json.tracks[i].audio == currentAudio){
      document.getElementById("song_title").innerHTML = json.tracks[i].title; //cambiar titulo
      document.getElementById("artwork").src = json.tracks[i].img; //cambiar artwork
      //Cambiar fondo
      document.getElementById("backImage").style = "background-image: linear-gradient(rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.7)), url('"+ json.tracks[i].img +"'); height: 941px;}"
      break;
    }
  }
}

async function changeBeat(json) {
  await audio.pause(); //Pausar audio anterior
  reload();
  audio = document.getElementById(currentAudio); //Establecer como audio el nuevo audio
  console.log(index);
  if(pauseCheck.getAttribute("class") != "fas fa-play play-btn icon-play hide"){ //Play al saltar cancion
    playPause.querySelector(".pause-btn").classList.toggle("hide");
    playPause.querySelector(".play-btn").classList.toggle("hide");
  }

  await audio.play();
  audioNbeat = document.getElementById("audio"+index+"beat");

  //change information

  for(var i = 0; i<4; i++){
    console.log(json.tracks[i])
    if(json.tracks[i].audio == currentAudio){
      console.log(currentAudio + " (" + json.tracks[i].title + ")")
      document.getElementById("song_title").innerHTML = json.tracks[i].title; //cambiar titulo
      document.getElementById("artwork").src = json.tracks[i].img; //cambiar artwork
      //Cambiar fondo
      document.getElementById("backImage").style = "background-image: linear-gradient(rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.7)), url('"+ json.tracks[i].img +"'); height: 941px;}"

    }

  }
  
}

function reload(){
  var container = document.getElementById("audios");
  var content = container.innerHTML;
  container.innerHTML= content; 
}

function selectorChanged(){

  var selectorValue = selector.value;

  console.log(selectorValue)

  switch(selectorValue){

    case "1": //Index debe ser 0-3
      selectedBeatpack = obj;
      console.log("Elegido beatpack2")
      index = 1;
      currentAudio = "audio0";
      break;

    case "2": //Index debe ser 4-7
      selectedBeatpack = december;
      console.log("Elegido beatpack2")
      index = 5;
      currentAudio = "audio4";
      break;

    case "3":
      selectedBeatpack = 3;
      break;

    case "4":
      selectedBeatpack = 4;
      break;

    case "5":
      selectedBeatpack = 5;
      break;

  }

  changeBeat(selectedBeatpack);

}

playPause.addEventListener("click", () => {

  if (audio.paused || audio.ended) {
    playPause.querySelector(".pause-btn").classList.toggle("hide");
    playPause.querySelector(".play-btn").classList.toggle("hide");
    audio.play();
  } else {
    audio.pause();
    playPause.querySelector(".pause-btn").classList.toggle("hide");
    playPause.querySelector(".play-btn").classList.toggle("hide");
  }

});

forward.addEventListener("click", () => {

  if(selectedBeatpack == obj){//Si es el beatpack 1

    if(index != 4){
      index = index + 1;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }else{
      index = 1
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }

  }else if(selectedBeatpack == december){ //Si es el beatpack 2

    if(index != 8){
      index = index + 1;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }else{
      index = 5
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }

  }

});

backward.addEventListener("click", () => {

  if(selectedBeatpack == obj){//Si es el beatpack 1

    if(index != 1){
      index = index - 1;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }else{
      index = 4;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }

  }else if(selectedBeatpack == december){ //Si es el beatpack 2

    if(index != 5){
      index = index - 1;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }else{
      index = 8;
      currentAudio = "audio" + (index - 1);
      changeBeat(selectedBeatpack);
    }

  }

});

document.body.onkeyup = function(e){
  if(e.keyCode == 32){
      
    if (audio.paused || audio.ended) {
      playPause.querySelector(".pause-btn").classList.toggle("hide");
      playPause.querySelector(".play-btn").classList.toggle("hide");
      audio.play();
    } else {
      audio.pause();
      playPause.querySelector(".pause-btn").classList.toggle("hide");
      playPause.querySelector(".play-btn").classList.toggle("hide");
    }

  }
}

// audio.addEventListener("ended", () => {

//   forwardSong();

// });

// function forwardSong(){

//   if(index+1 != beatList.length){
//     index = index + 1;
//     currentAudio = "audio" + index;
//     changeBeat();
//   }else{
//     index = 0
//     currentAudio = "audio0";
//     changeBeat();
//   }

// }

