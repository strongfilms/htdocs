class audioObject{

    audioName = "";
    image = "";
    mp3 = "";

    constructor(audioName, image, mp3){
        this.audioName = audioName;
        this.image = image;
        this.mp3 = mp3;
    }

    getAudioName(){
        return this.audioName;
    }

}